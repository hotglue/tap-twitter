"""Twitter tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_twitter.streams import (
    QuotetweetshStream,
    RetweetsStream,
    TweetLikesStream,
    TweetSearchStream,
    TweetStream,
)

STREAM_TYPES = [
    TweetStream,
    TweetSearchStream,
    TweetLikesStream,
    RetweetsStream,
    QuotetweetshStream,
]


class TapTwitter(Tap):
    """Twitter tap class."""

    name = "tap-twitter"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "bearer_token",
            th.StringType,
            required=True,
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapTwitter.cli()
