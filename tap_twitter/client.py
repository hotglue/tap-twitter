"""REST client handling, including TwitterStream base class."""

import time
from pathlib import Path
from typing import Any, Dict, Optional

import requests
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream


class TwitterStream(RESTStream):
    """Twitter stream class."""

    url_base = "https://api.twitter.com/2/tweets"

    records_jsonpath = "$.data[*]"  # Or override `parse_response`.
    next_page_token_jsonpath = "$.meta.next_token"  # Or override `get_next_page_token`.
    rules_applied = False
    _page_size = 100

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        headers["Authorization"] = f"Bearer {self.config.get('bearer_token')}"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        # Check and apply rate limit
        limit_remaining = int(float(response.headers["x-rate-limit-remaining"]))
        limit_reset_in = int(float(response.headers["x-rate-limit-reset"]))

        if limit_remaining <= 2:
            current_time_epoch = time.time()
            # get remaining seconds from epoch seconds sent by twitter
            limit_reset_in = int((current_time_epoch - limit_reset_in) * 60)
            time.sleep(limit_reset_in)

        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
        else:
            next_page_token = response.headers.get("X-Next-Page", None)

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["pagination_token"] = next_page_token

        params["max_results"] = self._page_size

        if self.name == "tweetsearch":
            params["query"] = self.config.get("query")
        params = self.get_selected_fields(params, self.metadata.items())
        return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        if self.rules_applied == False and self.name == "tweets":
            rules_to_delete = self.get_rules()
            self.delete_all_rules(rules_to_delete)
            self.set_rules(self.config.get("rules"))
            self.rules_applied = True
        return None

    def get_rules(self):
        response = requests.get(
            "https://api.twitter.com/2/tweets/search/stream/rules",
            headers=self.http_headers,
        )
        if response.status_code != 200:
            raise Exception(
                f"Cannot get rules (HTTP {response.status_code}): {response.text}"
            )
        return response.json()

    def delete_all_rules(self, rules):
        if rules is None or "data" not in rules:
            return None

        ids = list(map(lambda rule: rule["id"], rules["data"]))
        payload = {"delete": {"ids": ids}}
        response = requests.post(
            "https://api.twitter.com/2/tweets/search/stream/rules",
            headers=self.http_headers,
            json=payload,
        )
        if response.status_code != 200:
            raise Exception(
                f"Cannot delete rules (HTTP {response.status_code}): {response.text}"
            )

    def set_rules(self, rules):
        # You can adjust the rules if needed

        payload = {"add": rules}
        response = requests.post(
            "https://api.twitter.com/2/tweets/search/stream/rules",
            headers=self.http_headers,
            json=payload,
        )
        if response.status_code != 201:
            raise Exception(
                f"Cannot add rules (HTTP {response.status_code}): {response.text}"
            )

    def get_tweet_property_types(self):
        return_types = {}
        return_types["expansions"] = [
            "attachments.poll_ids",
            "attachments.media_keys",
            "author_id",
            "entities.mentions.username",
            "geo.place_id",
            "in_reply_to_user_id",
            "referenced_tweets.id",
            "referenced_tweets.id.author_id",
            "pinned_tweet_id",
        ]
        return_types["media.fields"] = [
            "duration_ms",
            "height",
            "media_key",
            "preview_image_url",
            "type",
            "url",
            "width",
            "public_metrics",
            "non_public_metrics",
            "organic_metrics",
            "promoted_metrics",
            "alt_text",
            "variants",
        ]
        return_types["place.fields"] = [
            "contained_within",
            "country",
            "country_code",
            "full_name",
            "geo",
            "id",
            "name",
            "place_type",
        ]
        return_types["poll.fields"] = [
            "duration_minutes",
            "end_datetime",
            "id",
            "options",
            "voting_status",
        ]
        return_types["tweet.fields"] = [
            "attachments",
            "author_id",
            "context_annotations",
            "conversation_id",
            "created_at",
            "entities",
            "geo",
            "id",
            "in_reply_to_user_id",
            "lang",
            "non_public_metrics",
            "public_metrics",
            "organic_metrics",
            "promoted_metrics",
            "possibly_sensitive",
            "referenced_tweets",
            "reply_settings",
            "source",
            "text",
            "withheld",
        ]
        return_types["user.fields"] = [
            "created_at",
            "description",
            "entities",
            "id",
            "location",
            "name",
            "pinned_tweet_id",
            "profile_image_url",
            "protected",
            "public_metrics",
            "url",
            "username",
            "verified",
            "withheld",
            # "withheld.country_codes",
            # "withheld.scope",
        ]
        return return_types

    def get_field_groups(self):
        groups = {}
        groups["tweetsearch"] = [
            "expansions",
            "media.fields",
            "place.fields",
            "poll.fields",
            "tweet.fields",
            "user.fields",
        ]
        groups["quotetweets"] = [
            "expansions",
            "media.fields",
            "place.fields",
            "poll.fields",
            "tweet.fields",
            "user.fields",
        ]
        groups["tweetlikes"] = ["expansions", "tweet.fields", "user.fields"]
        groups["retweets"] = ["expansions", "tweet.fields", "user.fields"]
        return groups

    def get_selected_fields(self, params, properties_metadata):
        selected_properties = {}
        tweet_types = self.get_tweet_property_types()
        field_group_names = self.get_field_groups()
        # Initialize blank array for each type
        for key, value in tweet_types.items():
            selected_properties[key] = []
        # prepare selected fields
        for key, value in properties_metadata:
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                for key, value in tweet_types.items():
                    if field_name in value:
                        selected_properties[key].append(field_name)

        for key, value in selected_properties.items():
            if key in field_group_names[self.name]:
                params[key] = ",".join(value)

        return params
