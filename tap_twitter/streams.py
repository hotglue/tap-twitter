"""Stream type classes for tap-twitter."""

import datetime
import json
from typing import Any, Dict, Iterable, Optional

import requests
from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_twitter.client import TwitterStream


class TweetStream(TwitterStream):
    """Define custom stream."""

    name = "tweet_stream_live"
    path = "/search/stream"
    primary_keys = ["tweet_id"]
    replication_key = "timestamp"
    schema = th.PropertiesList(
        th.Property("tweet_id", th.StringType),
        th.Property("timestamp", th.DateTimeType),
        th.Property("text", th.StringType),
        th.Property("tag_id", th.StringType),
        th.Property("tag_value", th.StringType),
    ).to_dict()

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:

        response = requests.get(
            "https://api.twitter.com/2/tweets/search/stream",
            headers=self.http_headers,
            stream=True,
        )
        if response.status_code != 200:
            raise Exception(
                f"Cannot get stream (HTTP {response.status_code}): {response.text}"
            )

        for response_line in response.iter_lines():
            if response_line:
                json_response = json.loads(response_line)

                record = {
                    "tweet_id": json_response["data"]["id"],
                    "timestamp": datetime.datetime.now().isoformat(),
                    "text": json_response["data"]["text"],
                    "tag_id": json_response["matching_rules"][0]["id"],
                    "tag_value": json_response["matching_rules"][0]["tag"],
                }
                transformed_record = self.post_process(record, context)
                if transformed_record is None:
                    # Record filtered out during post_process()
                    continue
                yield transformed_record


class TweetSearchStream(TwitterStream):
    """Define custom stream."""

    name = "tweetsearch"
    path = "/search/recent"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("text", th.StringType),
        th.Property("author_id", th.StringType),
        th.Property("conversation_id", th.StringType),
        th.Property("in_reply_to_user_id", th.StringType),
        th.Property("referenced_tweets", th.CustomType({"type": ["array", "string"]})),
        th.Property("referenced_tweets.type", th.StringType),
        th.Property("referenced_tweets.id.type", th.StringType),
        th.Property("attachments", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "attachments.media_keys", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property(
            "attachments.poll_ids", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("geo", th.CustomType({"type": ["object", "string"]})),
        th.Property("geo.coordinates", th.CustomType({"type": ["object", "string"]})),
        th.Property("geo.coordinates.type", th.StringType),
        th.Property(
            "geo.coordinates.coordinates", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("geo.place_id", th.StringType),
        th.Property(
            "context_annotations", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property(
            "context_annotations.domain", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("context_annotations.domain.id", th.StringType),
        th.Property("context_annotations.domain.name", th.StringType),
        th.Property("context_annotations.domain.description", th.StringType),
        th.Property(
            "context_annotations.entity", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("context_annotations.entity.id", th.StringType),
        th.Property("context_annotations.entity.name", th.StringType),
        th.Property("context_annotations.entity.description", th.StringType),
        th.Property("entities", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "entities.annotations", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("entities.annotations.start", th.NumberType),
        th.Property("entities.annotations.end", th.NumberType),
        th.Property("entities.annotations.probability", th.NumberType),
        th.Property("entities.annotations.type", th.StringType),
        th.Property("entities.annotations.normalized_text", th.StringType),
        th.Property("entities.urls", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.urls.start", th.NumberType),
        th.Property("entities.urls.end", th.NumberType),
        th.Property("entities.urls.url", th.StringType),
        th.Property("entities.urls.expanded_url", th.StringType),
        th.Property("entities.urls.display_url", th.StringType),
        th.Property("entities.urls.unwound_url", th.StringType),
        th.Property("entities.hashtags", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.hashtags.start", th.NumberType),
        th.Property("entities.hashtags.end", th.NumberType),
        th.Property("entities.hashtags.tag", th.StringType),
        th.Property("entities.mentions", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.mentions.start", th.NumberType),
        th.Property("entities.mentions.end", th.NumberType),
        th.Property("entities.mentions.username", th.StringType),
        th.Property("entities.cashtags", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.cashtags.start", th.NumberType),
        th.Property("entities.cashtags.end", th.NumberType),
        th.Property("entities.cashtags.tag", th.StringType),
        th.Property("withheld", th.CustomType({"type": ["object", "string"]})),
        th.Property("withheld.copyright", th.BooleanType),
        th.Property(
            "withheld.country_codes", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("withheld.scope", th.StringType),
        th.Property("public_metrics", th.CustomType({"type": ["object", "string"]})),
        th.Property("public_metrics.retweet_count", th.NumberType),
        th.Property("public_metrics.reply_count", th.NumberType),
        th.Property("public_metrics.like_count", th.NumberType),
        th.Property("public_metrics.quote_count", th.NumberType),
        # th.Property("non_public_metrics", th.CustomType({"type": ["object", "string"]})),
        # th.Property("non_public_metrics.impression_count", th.NumberType),
        # th.Property("non_public_metrics.url_link_clicks", th.NumberType),
        # th.Property("non_public_metrics.user_profile_clicks", th.NumberType),
        # th.Property("organic_metrics", th.CustomType({"type": ["object", "string"]})),
        # th.Property("organic_metrics.impression_count", th.NumberType),
        # th.Property("organic_metrics.url_link_clicks", th.NumberType),
        # th.Property("organic_metrics.user_profile_clicks", th.NumberType),
        # th.Property("organic_metrics.retweet_count", th.NumberType),
        # th.Property("organic_metrics.reply_count", th.NumberType),
        # th.Property("promoted_metrics", th.CustomType({"type": ["object", "string"]})),
        # th.Property("promoted_metrics.impression_count", th.NumberType),
        # th.Property("promoted_metrics.url_link_clicks", th.NumberType),
        # th.Property("promoted_metrics.user_profile_clicks", th.NumberType),
        # th.Property("promoted_metrics.retweet_count", th.NumberType),
        # th.Property("promoted_metrics.retweet_count", th.NumberType),
        # th.Property("promoted_metrics.like_count", th.NumberType),
        th.Property("possibly_sensitive", th.BooleanType),
        th.Property("lang", th.StringType),
        th.Property("reply_settings", th.StringType),
        th.Property("source", th.StringType),
        th.Property("includes", th.CustomType({"type": ["object", "string"]})),
        th.Property("includes.tweets", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.users", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.places", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.media", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.polls", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "tweet_id": record["id"],
        }


class TweetLikesStream(TwitterStream):
    """Define custom stream."""

    name = "tweetlikes"
    path = "/{tweet_id}/liking_users"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = TweetSearchStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("username", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("protected", th.BooleanType),
        th.Property("withheld", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "withheld.country_codes", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("withheld.scope", th.StringType),
        th.Property("location", th.StringType),
        th.Property("url", th.StringType),
        th.Property("description", th.StringType),
        th.Property("verified", th.BooleanType),
        th.Property("entities", th.CustomType({"type": ["object", "string"]})),
        th.Property("entities.url", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.url.urls", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.url.urls.start", th.NumberType),
        th.Property("entities.url.urls.end", th.NumberType),
        th.Property("entities.url.urls.url", th.StringType),
        th.Property("entities.url.urls.expanded_url", th.StringType),
        th.Property("entities.url.urls.display_url", th.StringType),
        th.Property(
            "entities.description", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property(
            "entities.description.urls", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("entities.description.urls.start", th.NumberType),
        th.Property("entities.description.urls.end", th.NumberType),
        th.Property("entities.description.urls.url", th.StringType),
        th.Property("entities.description.urls.expanded_url", th.StringType),
        th.Property("entities.description.urls.display_url", th.StringType),
        th.Property(
            "entities.description.hashtags",
            th.CustomType({"type": ["array", "string"]}),
        ),
        th.Property("entities.description.hashtags.start", th.NumberType),
        th.Property("entities.description.hashtags.end", th.NumberType),
        th.Property("entities.description.hashtags.hashtag", th.StringType),
        th.Property(
            "entities.description.mentions",
            th.CustomType({"type": ["object", "string"]}),
        ),
        th.Property("entities.description.mentions.start", th.NumberType),
        th.Property("entities.description.mentions.end", th.NumberType),
        th.Property("entities.description.mentions.username", th.StringType),
        th.Property(
            "entities.description.cashtags",
            th.CustomType({"type": ["array", "string"]}),
        ),
        th.Property("entities.description.cashtags.start", th.NumberType),
        th.Property("entities.description.cashtags.end", th.NumberType),
        th.Property("entities.description.cashtags.cashtag", th.StringType),
        th.Property("profile_image_url", th.StringType),
        th.Property("public_metrics", th.CustomType({"type": ["object", "string"]})),
        th.Property("public_metrics.followers_count", th.NumberType),
        th.Property("public_metrics.tweet_count", th.NumberType),
        th.Property("public_metrics.listed_count", th.NumberType),
        th.Property("pinned_tweet_id", th.StringType),
        th.Property("includes.tweets", th.CustomType({"type": ["array", "string"]})),
        th.Property("errors", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class RetweetsStream(TwitterStream):
    """Define custom stream."""

    name = "retweets"
    path = "/{tweet_id}/retweeted_by"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = TweetSearchStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("username", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("protected", th.BooleanType),
        th.Property("withheld", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "withheld.country_codes", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("withheld.scope", th.StringType),
        th.Property("location", th.StringType),
        th.Property("url", th.StringType),
        th.Property("description", th.StringType),
        th.Property("verified", th.BooleanType),
        th.Property("entities", th.CustomType({"type": ["object", "string"]})),
        th.Property("entities.url", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.url.urls", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.url.urls.start", th.NumberType),
        th.Property("entities.url.urls.end", th.NumberType),
        th.Property("entities.url.urls.url", th.StringType),
        th.Property("entities.url.urls.expanded_url", th.StringType),
        th.Property("entities.url.urls.display_url", th.StringType),
        th.Property(
            "entities.description", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property(
            "entities.description.urls", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("entities.description.urls.start", th.NumberType),
        th.Property("entities.description.urls.end", th.NumberType),
        th.Property("entities.description.urls.url", th.StringType),
        th.Property("entities.description.urls.expanded_url", th.StringType),
        th.Property("entities.description.urls.display_url", th.StringType),
        th.Property(
            "entities.description.hashtags",
            th.CustomType({"type": ["array", "string"]}),
        ),
        th.Property("entities.description.hashtags.start", th.NumberType),
        th.Property("entities.description.hashtags.end", th.NumberType),
        th.Property("entities.description.hashtags.hashtag", th.StringType),
        th.Property(
            "entities.description.mentions",
            th.CustomType({"type": ["object", "string"]}),
        ),
        th.Property("entities.description.mentions.start", th.NumberType),
        th.Property("entities.description.mentions.end", th.NumberType),
        th.Property("entities.description.mentions.username", th.StringType),
        th.Property(
            "entities.description.cashtags",
            th.CustomType({"type": ["array", "string"]}),
        ),
        th.Property("entities.description.cashtags.start", th.NumberType),
        th.Property("entities.description.cashtags.end", th.NumberType),
        th.Property("entities.description.cashtags.cashtag", th.StringType),
        th.Property("profile_image_url", th.StringType),
        th.Property("public_metrics", th.CustomType({"type": ["object", "string"]})),
        th.Property("public_metrics.followers_count", th.NumberType),
        th.Property("public_metrics.tweet_count", th.NumberType),
        th.Property("public_metrics.listed_count", th.NumberType),
        th.Property("pinned_tweet_id", th.StringType),
        th.Property("includes.tweets", th.CustomType({"type": ["array", "string"]})),
        th.Property("errors", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class QuotetweetshStream(TwitterStream):
    """Define custom stream."""

    name = "quotetweets"
    path = "/{tweet_id}/quote_tweets"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = TweetSearchStream

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("text", th.StringType),
        th.Property("author_id", th.StringType),
        th.Property("conversation_id", th.StringType),
        th.Property("in_reply_to_user_id", th.StringType),
        th.Property("referenced_tweets", th.CustomType({"type": ["array", "string"]})),
        th.Property("referenced_tweets.type", th.StringType),
        th.Property("referenced_tweets.id.type", th.StringType),
        th.Property("attachments", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "attachments.media_keys", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property(
            "attachments.poll_ids", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("geo", th.CustomType({"type": ["object", "string"]})),
        th.Property("geo.coordinates", th.CustomType({"type": ["object", "string"]})),
        th.Property("geo.coordinates.type", th.StringType),
        th.Property(
            "geo.coordinates.coordinates", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("geo.place_id", th.StringType),
        th.Property(
            "context_annotations", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property(
            "context_annotations.domain", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("context_annotations.domain.id", th.StringType),
        th.Property("context_annotations.domain.name", th.StringType),
        th.Property("context_annotations.domain.description", th.StringType),
        th.Property(
            "context_annotations.entity", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("context_annotations.entity.id", th.StringType),
        th.Property("context_annotations.entity.name", th.StringType),
        th.Property("context_annotations.entity.description", th.StringType),
        th.Property("entities", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "entities.annotations", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("entities.annotations.start", th.NumberType),
        th.Property("entities.annotations.end", th.NumberType),
        th.Property("entities.annotations.probability", th.NumberType),
        th.Property("entities.annotations.type", th.StringType),
        th.Property("entities.annotations.normalized_text", th.StringType),
        th.Property("entities.urls", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.urls.start", th.NumberType),
        th.Property("entities.urls.end", th.NumberType),
        th.Property("entities.urls.url", th.StringType),
        th.Property("entities.urls.expanded_url", th.StringType),
        th.Property("entities.urls.display_url", th.StringType),
        th.Property("entities.urls.unwound_url", th.StringType),
        th.Property("entities.hashtags", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.hashtags.start", th.NumberType),
        th.Property("entities.hashtags.end", th.NumberType),
        th.Property("entities.hashtags.tag", th.StringType),
        th.Property("entities.mentions", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.mentions.start", th.NumberType),
        th.Property("entities.mentions.end", th.NumberType),
        th.Property("entities.mentions.username", th.StringType),
        th.Property("entities.cashtags", th.CustomType({"type": ["array", "string"]})),
        th.Property("entities.cashtags.start", th.NumberType),
        th.Property("entities.cashtags.end", th.NumberType),
        th.Property("entities.cashtags.tag", th.StringType),
        th.Property("withheld", th.CustomType({"type": ["object", "string"]})),
        th.Property("withheld.copyright", th.BooleanType),
        th.Property(
            "withheld.country_codes", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("withheld.scope", th.StringType),
        th.Property("public_metrics", th.CustomType({"type": ["object", "string"]})),
        th.Property("public_metrics.retweet_count", th.NumberType),
        th.Property("public_metrics.reply_count", th.NumberType),
        th.Property("public_metrics.like_count", th.NumberType),
        th.Property("public_metrics.quote_count", th.NumberType),
        th.Property("possibly_sensitive", th.BooleanType),
        th.Property("lang", th.StringType),
        th.Property("reply_settings", th.StringType),
        th.Property("source", th.StringType),
        th.Property("includes", th.CustomType({"type": ["object", "string"]})),
        th.Property("includes.tweets", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.users", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.places", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.media", th.CustomType({"type": ["array", "string"]})),
        th.Property("includes.polls", th.StringType),
    ).to_dict()
